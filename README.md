# AWS Cloud Development Kit - S3 Bucket Creation

**S3 (Simple Storage Service)** is a scalable cloud storage service that enables storing and retrieving any amount of data, anytime, from anywhere on the web. An **S3 Bucket** is a container in S3 for data storage. Within S3 buckets, data is organized into **objects**, each identified by a unique key (akin to a file path), allowing for efficient data management and retrieval. <br>

*S3 buckets offer secure, durable, and scalable storage, making them ideal for various applications, from website hosting to data backups.*

<div align="center">
  <img src="https://miro.medium.com/v2/resize:fit:720/format:webp/0*aJ6BpnaRXbf01pEB.png">
</div>


## Prerequisites

Before you begin, ensure you have met the following requirements:

1. **AWS Account**: Sign up at [AWS](http://aws.amazon.com) if you don't already have an account.
2. **AWS CLI**: Ensure AWS CLI is installed and configured. Use the following commands to verify installation and configure your AWS CLI if you haven't already:
   - Verify Installation: `aws --version`
   - Configure AWS CLI: `aws configure`
   - Check Configuration: `aws configure list`
   - Installation Command (macOS):
     ```bash
     curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
     sudo installer -pkg AWSCLIV2.pkg -target /
     ```
3. **Node.js & npm**: Node.js is required for the AWS CDK CLI. Use the following commands to verify installation:
   - Verify Installation: `node --version && npm --version`
   - Installation Command (macOS): `brew install node`

4. **Python 3**: Python is used for the CDK project.
   - Verify Installation: `python --version`
   - Installation Command (macOS): `brew install python`

5. **AWS CDK**: The AWS Cloud Development Kit (CDK) is used to define cloud infrastructure in code.
   - Verify Installation: `cdk --version`
   - Installation Command: `sudo npm install -g aws-cdk`
   
6. **IDE**: Use your preferred IDE, such as Visual Studio Code (VS Code), for development.

## Setting Up Your Development Environment

### Creating a Virtual Environment

Create and activate a Python virtual environment for project dependencies:

```bash
python3 -m venv .venv
source .venv/bin/activate
```

### Initializing a CDK Project

Initialize a new CDK project using Python as the language:

```bash
cdk init app --language python
```

### Installing Dependencies

Install the required Python packages specified in `requirements.txt`:

```bash
python -m pip install -r requirements.txt
```

## Deployment

### Bootstrap Your AWS Environment (if required)

If this is your first time deploying a CDK app in the AWS region, bootstrap your AWS environment:

```bash
cdk bootstrap
```

### Deploy Your CDK App

Before deploying, do not forget to give "AdministratorAccess" to the user:

Services -> IAM -> Users -> *username* -> Add Permissions -> Add Permissions -> Attach Policies Directly -> *AdministratorAccess* -> Next -> Add Permissions.

Now, you can deploy your CDK stack to your AWS account:

```bash
cdk deploy
```

Here is our bucket (Services -> Storage -> S3):

<div align="center">
  <img src="https://i.ibb.co/6s8SqLf/Screenshot-2024-02-16-at-2-49-28-PM.png">
</div>


## Additional Information

1. You can use **AWS CodeWhisperer** for real-time code recommendations while developing your CDK application. To use CodeWhisperer in VSCode, follow these steps:

- *Install the AWS Toolkit extension*: Open the Extensions view by clicking on the square icon on the sidebar or pressing `Ctrl+Shift+X`. Search for "AWS Toolkit" and install it.
- *Enable CodeWhisperer*: After installing the AWS Toolkit, go to the extension settings and ensure CodeWhisperer is enabled.
- *Start Coding*: Begin writing your CDK code in a Python file. As you type, CodeWhisperer will provide suggestions (you can manually trigger CodeWhisperer by using Option+C [MacOS]) based on your context and comments. Press `Tab` or `Enter` (depending on your configuration) to accept a suggestion.

<div align="center">
  <img src="https://i.ibb.co/LJNNGkb/Screenshot-2024-02-16-at-2-41-17-PM.png">
</div>


2. Remember to deactivate your virtual environment when you're done working on the project:
```bash
deactivate
```
3. Ensure to regularly commit changes to your version control system and push to your repository to keep track of your development progress.
