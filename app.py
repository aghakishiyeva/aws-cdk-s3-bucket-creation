#!/usr/bin/env python3
import aws_cdk as cdk
from s3bucket.s3bucket_stack import S3Bucket_Stack

app = cdk.App()
S3Bucket_Stack(app, "S3BucketStack")
app.synth()