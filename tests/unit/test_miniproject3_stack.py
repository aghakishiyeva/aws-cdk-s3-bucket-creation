import aws_cdk as core
import aws_cdk.assertions as assertions

from s3bucket.s3bucket_stack import Miniproject3Stack

# example tests. To run these tests, uncomment this file along with the example
# resource in miniproject3/miniproject3_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = Miniproject3Stack(app, "miniproject3")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
