from aws_cdk import (
    aws_s3 as s3,
    Stack,
    CfnOutput
)
from constructs import Construct

class S3Bucket_Stack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Define the S3 bucket with versioning and server-side encryption
        bucket = s3.Bucket(self, "MyBucket",
            bucket_name="bucket-aghakishiyeva", 
            versioned=True,
            encryption=s3.BucketEncryption.S3_MANAGED
        )

        # Outputs the bucket name to be easily found after deployment
        CfnOutput(self, "BucketNameOutput", value=bucket.bucket_name)
